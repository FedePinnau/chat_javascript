import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js' //importamos funciones que estan escritas en javascript//
import {getAuth, createUserWithEmailAndPassword,} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js' //importamos funciones que estan escritas en javascript//


// Vinculamos el archivo con nuestro Firebase y llamamos funciones para usar en éste archivo
const firebaseConfig = {
  apiKey: "AIzaSyAMmtU4UasGLtpAFt7uhD4UE3phYxX1be0",
  authDomain: "foroea-f44d7.firebaseapp.com",
  databaseURL: "https://foroea-f44d7-default-rtdb.firebaseio.com",
  projectId: "foroea-f44d7",
  storageBucket: "foroea-f44d7.appspot.com",
  messagingSenderId: "628845464517",
  appId: "1:628845464517:web:5ec3e525d5a24eb7d8d9ac"
};

// Inicializamos Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app)
const database = getDatabase(app)
//Creo las variables en las que voy a guardar los datos
let correoRegRef = document.getElementById("correoRegID");
let passRegRef = document.getElementById("passRegID");
let botonRegRef = document.getElementById("botonRegID");
let nameRegRef = document.getElementById("nameRegID")
let surnameRegRef = document.getElementById("surnameRegID")
let cityRegRef = document.getElementById("cityRegID")
botonRegRef.addEventListener("click", registerUser);

function registerUser(){//Ésta función es para registrar al usuario

  let email = correoRegRef.value
  let pass = passRegRef.value
  var name = nameRegRef.value
  var surname = surnameRegRef.value
  var city = cityRegRef.value

  createUserWithEmailAndPassword(auth, email, pass, name, surname, city) 
  
  .then((userCredential) => { //guarda la información escrita en los campos en la base de datos de Firebase
    const user = userCredential.user;
    const uid = user.uid;

    console.log("UID: " + uid)
    
    set(ref(database, 'usuarios/' + uid), {
      nombre: name, apellido: surname, ciudad:city
    })
    console.log("Registrado correctamente")
  })
  .catch((error) => { //cualquier error termina en el catch
    const errorCode = error.code;
    const errorMessage = error.message;
    // ..
    console.log("Error")
  });
}