import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue, query, orderByChild, child, get,} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js' //importamos funciones que estan escritas en javascript//
import {getAuth, onAuthStateChanged, signOut} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js' //importamos funciones que estan escritas en javascript//
//vinculamos con firebase y llamamos funciones
const firebaseConfig = {
  apiKey: "AIzaSyAMmtU4UasGLtpAFt7uhD4UE3phYxX1be0",
  authDomain: "foroea-f44d7.firebaseapp.com",
  databaseURL: "https://foroea-f44d7-default-rtdb.firebaseio.com",
  projectId: "foroea-f44d7",
  storageBucket: "foroea-f44d7.appspot.com",
  messagingSenderId: "628845464517",
  appId: "1:628845464517:web:5ec3e525d5a24eb7d8d9ac"
};
  const app = initializeApp(firebaseConfig);
  const auth = getAuth();
  
  const user = auth.currentUser;
  
  const database = getDatabase();
  
  var nombre;
  
  //vinculamos los botones de la página con el archivo de js:
  var mensajeRef = document.getElementById("textoChatId");
  var chatRef = document.getElementById("mensajeChatId");
  
  var enviarRef = document.getElementById("botonChatId");
  enviarRef.addEventListener("click", enviarMensaje);
  
  var logOut = document.getElementById("BotonlogoutID");
  logOut.addEventListener("click", loggingOut);
  
  let uid;
  let correo;
  
  onAuthStateChanged(auth, (user) => { //ésta función sirve como un filtro entre usuarios con sesión iniciada y los que no
      if (user) {
        uid = user.uid;
        correo = user.email;
        console.log("Usuario es:" + " " + correo + "UID: " + uid);
  
      } else {
      }
  });
  
  
  function enviarMensaje(){
      let mensaje = mensajeRef.value;
  
      push(ref(database, "usuarios/mensaje/"), { //con el push cargamos los mensajes a la base de datos
          msg: mensaje,
          email: correo,
      })
  
      mensajeRef.value = "";
      console.log("Mensaje enviado. Correo: " + correo);
  }
  
  const queryMensajes = query(ref(database, 'usuarios/mensaje/'), orderByChild('email'));
  console.log(queryMensajes);
  //el query se usa para elegir ciertos datos que queremos cargar
  onValue(queryMensajes, (snapshot) => { //con onValue conseguimos los mensajes de la base de datos para despues mostrarlos en el chat
      snapshot.forEach((childSnapshot) => {
        const childKey = childSnapshot.key;
        const childData = childSnapshot.val().msg;
        console.log(childData);
        
        chatRef.innerHTML += `
            <p>${childSnapshot.val().email}: ${childSnapshot.val().msg}</p>
        `;
      });
  });
  
  function loggingOut(){ //ésta función es para cerrar la sesión del usuario iniciada
  
    const auth = getAuth();
  signOut(auth).then(() => {
    window.location.href ="../index.html";
  }).catch((error) => {
  });
  
  }
  
