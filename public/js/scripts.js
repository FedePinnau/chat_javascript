import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-app.js'
import { getDatabase, ref, set, push, onValue} from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-database.js' //importamos funciones que estan escritas en javascript//
import {getAuth, createUserWithEmailAndPassword,  signInWithEmailAndPassword } from 'https://www.gstatic.com/firebasejs/9.8.1/firebase-auth.js' //importamos funciones que estan escritas en javascript//


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyAMmtU4UasGLtpAFt7uhD4UE3phYxX1be0",
  authDomain: "foroea-f44d7.firebaseapp.com",
  databaseURL: "https://foroea-f44d7-default-rtdb.firebaseio.com",
  projectId: "foroea-f44d7",
  storageBucket: "foroea-f44d7.appspot.com",
  messagingSenderId: "628845464517",
  appId: "1:628845464517:web:5ec3e525d5a24eb7d8d9ac"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const auth = getAuth(app)

let correoLogRef = document.getElementById("correoLogInID");
let passLogRef = document.getElementById("passLogInID");
let botonLogRef = document.getElementById("botonLogID");
botonLogRef.addEventListener("click", logUser);


function logUser(){

let email = correoLogRef.value;
let password = passLogRef.value;

signInWithEmailAndPassword(auth, email, password)
  .then((userCredential) => {
    // Signed in
    const user = userCredential.user;
    window.location.href="./pages/chat.html", 'about'
    console.log("Iniciado sesión correctamente")
  })
  .catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
  });
 }
